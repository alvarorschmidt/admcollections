<style>
    #homepage {
        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        align-items: center;
    }
    #banner {
        width: 100%;
        height: 200px;
        background-color: var(--sild);
    }
    #newspaper {
        flex-grow: 1;
        width: 100%;
        display: flex;
        justify-content: space-around;
        align-items: center;
    }
    .news {
        width: 225px;
        height: 100px;
        background-color: var(--neutral);
        border-radius: 6px;
        display: flex;
        flex-direction: column;
        justify-content: center;
    }
    .news-title {
        border-bottom-style: solid;
        border-bottom-width: thin;
        text-align: center;
        font-family: "trebuchet MS", sans-serif;
        font-weight:bold;
        font-size: 16px;
    }
    .news-content {
        height: 100%;
        position: relative;
    }
    .news-content_numeric > span {
        position: absolute;
        left: 50%;
        top: 45%;
        transform: translate(-50%, -50%);
        text-align: center;
        font-size: 26px;
    }
    .news-content_text {
        padding-top: 5px;
        text-align: center;
        font-size: 16px;
    }
</style>
<div id="homepage">
    <div id="banner">
    </div>
    <div id="newspaper">
        <div class="news">
            <div class="news-title">Cole��es cadastradas.</div>
            <div class="news-content news-content_numeric">
                <span>00</span>
            </div>
        </div>
        <div class="news">
            <div class="news-title">Itens cadastrados.</div>
            <div class="news-content news-content_numeric">
                <span>00</span>
            </div>
        </div>
        <div class="news">
            <div class="news-title">Cole��o com mais itens.</div>
            <div class="news-content news-content_text">
                <span>NOME DE COLECAO</span>
            </div>
        </div>
    </div>
</div>