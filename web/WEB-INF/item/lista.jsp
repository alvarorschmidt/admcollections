<%@page import="br.feevale.admcollections.entity.Colecao"%>
<%@page import="java.util.List"%>
<%@page import="br.feevale.admcollections.entity.Item"%>
<style>
    #filter {
        width: 100%;
        height: 30px;
        box-sizing: border-box;
        padding: 0 10px;
        background-color: #DCD0C0;
        display: flex;
        justify-content: flex-end;
        align-items: center;
    }
    .filter-widgets {
        display: inline-block;
    }
    #table {
        display: flex;
        justify-content: center;
        flex-wrap: wrap;
    }
    .card {
        float: left;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        width: 150px;height: 190px;
        box-sizing: border-box;
        border-radius: 3px;
        margin: 5px;padding: 6px 4px 6px 4px;
        font-family: "Consolas", Helvetica, sans-serif;
        background-color: white;
    }
    .card-header {
        display: flex;
        justify-content: center;
    }
    .card-id {
        padding: 0px 2px 0px 2px;
    }
    .card-title {
        flex: 1;
        text-align: center;
        background-color: gray;
    }
    .card-content {
        font-size: 11px;
    }
    .card-footer {
        display: flex;
        justify-content: space-between;
    }
    .card-btn  {
        display: block;
        width: 65px;height: 25px;
        background: graytext;
        text-align: center;
        border-radius: 5px;
        color: white;
        text-decoration: none;
        font-size: 14px;
    }
</style>
<div id="filter">
    <% String submitUrl = (String) request.getAttribute("submitUrl");%>
    <form action="<%=submitUrl%>" method="post">
        <div class="filter-widgets">
            <select name="idColecao">
                <% int filtroDeColecaoAtivo = (int) request.getAttribute("filtroDeColecaoAtivo"); %>
                <option value="0" <%=filtroDeColecaoAtivo == 0 ? "selected" : ""%>>Todas</option>
                <% List<Colecao> colecoes = (List<Colecao>) request.getAttribute("colecoes");
                    for (Colecao colecao : colecoes) {%>                           
                    <option value="<%=colecao.getId()%>" <%=filtroDeColecaoAtivo == colecao.getId() ? "selected" : ""%>><%=colecao.getName()%></option>
                <% }%>
            </select>
            <button type="submit" class="button">Procurar</button>
        </div>
    </form>
</div>
<div id="table">
    <% List<Item> itens = (List<Item>) request.getAttribute("itens");
        for (Item item : itens) {%>
    <div class="card">
        <div class="card-header">
            <span class="card-id"><%=item.getId()%></span>
            <span class="card-title"><%=item.getName()%></span>
        </div>
        <div class="card-content">
            <p>
                <%=item.getDescription()%>
            </p>
        </div>
        <div class="card-footer">
            <a class="card-btn card-btn-update" href="<%=request.getContextPath()%>/item/atualizar?id=<%=item.getId()%>">Atualizar</a>
            <a class="card-btn card-btn-delete" href="<%=request.getContextPath()%>/item/remover?id=<%=item.getId()%>">Remover</a>
        </div>
    </div>
    <% }%>
</div>
