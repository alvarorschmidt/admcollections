<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ADM Collections</title>
        <style>
            html, body {
                --pale-gold: #C0B283;
                --sild: #DCD0C0;
                --paper: #F4F4F4;
                --charcoal: #373737;
                --neutral: #FFFFFF;
                --main-bg-color: var(--paper);
                --main-inverted-color: var(--charcoal);
                width: 100%; 
                height: 100%;
            }
            body { 
                margin: 0; 
                padding: 0;
                background-color: var(--main-bg-color);
                display: flex;
                flex-direction: column;
                justify-content: space-between;
                align-items: center;
            }
            header {
                padding: 15px 0;
                background-color: var(--main-inverted-color);
                width: 100%;
            }
            .header-title {
                display: block;
                text-align: center;
            }
            .header-title a {
                text-decoration: none;
                font-size: 34px;
                font-weight:bold;
                color: white;
            }
            .header-nav {
                display: flex;
                justify-content: center;
                margin: 0;
                padding: 0;
            }
            .header-nav ul {
                list-style-type: none;
                padding: 0;
            }
            .header-nav li {
                display: inline;
            }
            .header-nav a {
                color: white;
                text-align: center;
                font-size: 20px;
                font-weight:bold;
                text-decoration: none;
                padding: 14px 16px;
                padding-bottom: 2px;
                cursor: grab;
            }
            .header-nav a:hover {
                border-bottom: 1px solid gray;
            }
            #erroMsg {
                width: 100%;
                height: 25px;
                background-color: red;
                text-align: center;
            }
            .erroMsg-btn-close {
                background-color: transparent;
                border: none;
                cursor: pointer;
            }
            #content {
                flex-grow: 1;
                width: 100%;
                position: relative;
            }
            #footer {
                width: 100%;
                height: 30px;
                bottom: 0;
                background-color: var(--sild);
                text-align: center;
                font-size: 16px;
                color: black;
            }
        </style>
    </head>
    <body>
        <header>
            <span class="header-title"><a href="<%=request.getContextPath()%>">ADM Collections</a></span>
            <div class="header-nav">
                <ul>
                    <li><a href="<%=request.getContextPath()%>/colecao/buscar">Coleções</a></li>
                    <li><a href="<%=request.getContextPath()%>/colecao/cadastrar">Cadastrar Coleção</a></li>
                    <li><a href="<%=request.getContextPath()%>/item/buscar">Itens</a></li>
                    <li><a href="<%=request.getContextPath()%>/item/cadastrar">Cadastrar Item</a></li>
                </ul>
            </div>
        </header>
        <% String erroMsg = (String)request.getAttribute("erroMsg");
            if (erroMsg != null) { %>
        <div id="erroMsg">
            <%=erroMsg%>
            <button class="erroMsg-btn-close">&#10006;</button>
        </div>
        <% }%>
        <div id="content">
            <jsp:include page="${contentPage}"></jsp:include>
        </div>
        <div id="footer">
            <span class="footer-text">DEFAULT</span>
        </div>
        <script>
            var footerElement = document.getElementsByClassName('footer-text')[0];
            var footerMsg = 'desenvolvido por Álvaro, David.';
            var footerPrintedMsgLength = 0;
            var footerInterval = setInterval(function () {
                footerElement.innerHTML = footerMsg.slice(0, footerPrintedMsgLength++);
                if (footerPrintedMsgLength > footerMsg.length)
                    clearInterval(footerInterval);
            }, 75);
            
            var erroMsgBtnCloseElement = document.getElementsByClassName('erroMsg-btn-close')[0];
            erroMsgBtnCloseElement.onclick = function () {
                erroMsgElement = document.getElementById('erroMsg');
                erroMsgElement.setAttribute("style", "display: none;");
            }
        </script>
    </body>
</html>
