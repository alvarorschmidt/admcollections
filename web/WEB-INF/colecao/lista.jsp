<%@page import="br.feevale.admcollections.entity.Colecao"%>
<%@page import="java.util.List"%>
<style>
    #table {
        padding: 10px;
        display: flex;
        justify-content: center;
    }
    table {
        background: white;
        padding: 10px;
        box-sizing: border-box;
        /*        border-collapse: collapse;*/
        text-align: center;
        vertical-align: middle;
    }
    table, th, td {
        border: 1px solid black;
    }
    tr:hover {
        background-color: gray;
    }
    th {
        height: 25px;
    }
    .table-btn-box {
        display: flex;
        justify-content: space-between;
    }
    .table-btn {
        display: block;
        width: 65px;
        height: 25px;
        background: graytext;
        text-align: center;
        border-radius: 3px;
        margin: 2px;
        color: white;
        text-decoration: none;
        font-size: 14px;
    }
</style>
<div id="table">
    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Nome</th>
                <th>Descri��o</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <% List<Colecao> colecoes = (List<Colecao>) request.getAttribute("colecoes");
                for (Colecao colecao : colecoes) {%>
            <tr>
                <td><%=colecao.getId()%></td>
                <td><%=colecao.getName()%></td>
                <td><%=colecao.getDescription()%></td>
                <td class="table-btn-box">
                    <a class="table-btn table-btn-update" href="<%=request.getContextPath()%>/colecao/atualizar?id=<%=colecao.getId()%>">Atualizar</a>
                    <a class="table-btn table-btn-delete" href="<%=request.getContextPath()%>/colecao/remover?id=<%=colecao.getId()%>">Remover</a>
                </td>
            </tr>
            <% }%>
        </tbody>
    </table>
</div>
