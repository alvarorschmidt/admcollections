<%@page import="br.feevale.admcollections.entity.Colecao"%>
<style>
    #detalhe {
        height: 100%;
        display: flex;
        align-items: flex-start;
        justify-content: center;
        padding-top: 10px;
    }
    form {
        width: 400px;
        background: white;
        border-radius: 3px;
        box-sizing: border-box;
        padding: 10px;
    }
    form div + div {
        margin-top: 1em;
    }
    label {
        display: inline-block;
        width: 90px;
        text-align: right;
    }
    input {
        font: 1em sans-serif;
        width: 275px;
        box-sizing: border-box;
        border: 1px solid #999;
    }
    input:focus, textarea:focus {
        border-color: #000;
    }
    .detalhe-command-box {
        display: flex;
        justify-content: center;
    }
    .button {
        width: 150px;
    }
</style>
<div id="detalhe">
    <% Colecao colecao = (Colecao)request.getAttribute("colecao"); 
       String submitUrl = (String)request.getAttribute("submitUrl"); %>
    <form action="<%=submitUrl%>" method="post">
        <div>
            <label for="id">Id: </label>
            <input type="text" name="id" value="${colecao.getId()}">
        </div>
        <div>
            <label for="name">Nome: </label>
            <input type="text" name="name" value="${colecao.getName()}">
        </div>
        <div>
            <label for="description">Descri��o: </label>
            <input type="text" name="description" value="${colecao.getDescription()}">
        </div>
        <div class="detalhe-command-box">
            <button type="submit" class="button">Salvar</button>
        </div>  
    </form>    
</div>
