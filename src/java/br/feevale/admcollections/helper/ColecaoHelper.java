package br.feevale.admcollections.helper;

import br.feevale.admcollections.entity.Colecao;
import javax.servlet.http.HttpServletRequest;

public class ColecaoHelper {
    
    public static Colecao getColecaoFromRequest(HttpServletRequest request) {
        Colecao colecao = new Colecao();
        
        String id = (String) request.getParameter("id");
        String name = (String) request.getParameter("name");
        String description = (String) request.getParameter("description");
        
        try {
            colecao.setId(Integer.parseInt(id));
        } catch (Exception e) {
            colecao.setId(null);
        }
        colecao.setName(name);
        colecao.setDescription(description);
        
        return colecao;
    }
    
    public static void setColecaoOnRequest(Colecao colecao, HttpServletRequest request) {
        request.setAttribute("colecao", colecao);
    }
    
}
