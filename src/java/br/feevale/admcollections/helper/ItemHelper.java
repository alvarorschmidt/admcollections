package br.feevale.admcollections.helper;

import br.feevale.admcollections.entity.Item;
import javax.servlet.http.HttpServletRequest;

public class ItemHelper {
    
    public static Item getItemFromRequest(HttpServletRequest request) {
        Item item = new Item();
        
        String id = (String) request.getParameter("id");
        String idColecao = (String) request.getParameter("idColecao");
        String name = (String) request.getParameter("name");
        String description = (String) request.getParameter("description");
        
        try {
            item.setId(Integer.parseInt(id));
        } catch(Exception e) {
            item.setId(null);
        }
        try {
            item.setIdColecao(Integer.parseInt(idColecao));
        } catch(Exception e) {
            item.setIdColecao(null);
        }
        item.setName(name);
        item.setDescription(description);
        
        return item;
    }
    
    public static void setItemOnRequest(Item item, HttpServletRequest request) {
        request.setAttribute("item", item);
    }
    
}
