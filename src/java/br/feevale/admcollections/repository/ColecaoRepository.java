package br.feevale.admcollections.repository;

import br.feevale.admcollections.entity.Colecao;
import java.util.List;

public interface ColecaoRepository {
    
    List<Colecao> listAll();
    
    Colecao getById(Integer id);
    
    Colecao save(Colecao colecao);
    
    void remove(Integer id);
    
}
