package br.feevale.admcollections.repository;

import br.feevale.admcollections.entity.Colecao;
import br.feevale.admcollections.repository.ColecaoRepository;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ColecaoRepositoryMockImp implements ColecaoRepository {

    private static List<Colecao> colecoes;

    public ColecaoRepositoryMockImp() {
        super();
        this.colecoes = getColecoes();
    }
    
    @Override
    public List<Colecao> listAll() {
        return colecoes;
    }

    @Override
    public Colecao getById(Integer id) {
        List<Colecao> colecoesEncontradas = colecoes.stream().filter(colecao -> colecao.getId() == id).collect(Collectors.toList());
        if(colecoesEncontradas.isEmpty()) {
            return null;
        }
        else {
            return colecoesEncontradas.get(0);
        }
    }

    @Override
    public Colecao save(Colecao colecao) {
        if(colecao.getId() == null) {
            colecao.setId(getProximoId());
            colecoes.add(colecao);
        }
        else {
            remove(colecao.getId());
            colecoes.add(colecao);
        }
        
        return colecao;
    }

    @Override
    public void remove(Integer id) {
        Colecao colecaoSendoRemovida = getById(id);
        if(colecaoSendoRemovida != null) {
            colecoes.remove(colecaoSendoRemovida);
        }
    }
    
    private List<Colecao> getColecoes() {
        List<Colecao> colecaos = new ArrayList<>();
        for(int numeroColecao = 1; numeroColecao <= 10; numeroColecao++) {
            Colecao colecao = new Colecao();
            colecao.setId(numeroColecao);
            colecao.setName("Colecao " + numeroColecao);
            colecao.setDescription(String.format("Esta foi a %d colecao a ser criada.", numeroColecao));
            colecaos.add(colecao);
        }
        
        return colecaos;
    } 
    
    private Integer getProximoId() {
        return colecoes.stream().sorted(Comparator.comparing(Colecao::getId).reversed()).collect(Collectors.toList()).get(0).getId() + 1;
    }
}
