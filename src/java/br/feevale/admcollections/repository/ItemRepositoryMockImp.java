package br.feevale.admcollections.repository;

import br.feevale.admcollections.entity.Item;
import br.feevale.admcollections.repository.ItemRepository;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ItemRepositoryMockImp implements ItemRepository {

    private static List<Item> itens;

    public ItemRepositoryMockImp() {
        itens = getItems();
    }

    @Override
    public List<Item> listAll() {
        return itens;
    }
    
    @Override
    public List<Item> listByIdColecao(Integer idColecao) {
        return itens.stream().filter(item -> item.getIdColecao().compareTo(idColecao) == 0).collect(Collectors.toList());
    }

    @Override
    public Item getById(Integer id) {
        // return getItems().stream().filter(item -> item.getId() == id).limit(1).
        List<Item> itensEncontrados = itens.stream().filter(item -> item.getId() == id).collect(Collectors.toList());
        if (itensEncontrados.isEmpty()) {
            return null;
        } else {
            return itensEncontrados.get(0);
        }
    }

    @Override
    public Item save(Item item) {
        if (item.getId() == null) {
            item.setId(getProximoId());
            itens.add(item);
        } else {
            remove(item.getId());
            itens.add(item);
        }

        return item;
    }

    @Override
    public void remove(Integer id) {
        Item itemSendoRemovido = getById(id);
        if (itemSendoRemovido != null) {
            itens.remove(itemSendoRemovido);
        }
    }

    private List<Item> getItems() {
        List<Item> items = new ArrayList<>();
        for (int numeroItem = 1; numeroItem <= 10; numeroItem++) {
            Item item = new Item();
            item.setId(numeroItem);
            item.setName("Item " + numeroItem);
            item.setDescription("Descricao do item " + numeroItem);
            items.add(item);
        }

        return items;
    }

    private Integer getProximoId() {
        return itens.stream().sorted(Comparator.comparing(Item::getId).reversed()).collect(Collectors.toList()).get(0).getId() + 1;
    }
}
