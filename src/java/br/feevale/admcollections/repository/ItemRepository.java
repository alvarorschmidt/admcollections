package br.feevale.admcollections.repository;

import br.feevale.admcollections.entity.Item;
import java.util.List;

public interface ItemRepository {
    
    List<Item> listAll();
    
    List<Item> listByIdColecao(Integer idColecao);
    
    Item getById(Integer id);
    
    Item save(Item item);
    
    void remove(Integer id);
    
}
