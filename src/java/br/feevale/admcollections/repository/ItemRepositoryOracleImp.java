package br.feevale.admcollections.repository;

import br.feevale.admcollections.entity.Item;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ItemRepositoryOracleImp implements ItemRepository {

    private final String CONNECTION_STRING = "jdbc:oracle:thin:@localhost:1521:XE";
    private final String USER = "ADM_COLLECTION";
    private final String PASS = "ADMPASSWORD";

    public ItemRepositoryOracleImp() {
        try {
            Class.forName("oracle.jdbc.OracleDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Item> listAll() {
        ArrayList<Item> itens = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(CONNECTION_STRING, USER, PASS)) {
            StringBuilder sql = new StringBuilder();
            sql.append(" SELECT ");
            sql.append(getItemColumns());
            sql.append(" FROM ITEM ");

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql.toString());

            while (rs.next()) {
                Item item = new Item();
                item.setId(rs.getInt("ID_ITEM"));
                item.setIdColecao(rs.getInt("ID_COLECAO"));
                item.setName(rs.getString("NOME"));
                item.setDescription(rs.getString("DESCRICAO"));

                itens.add(item);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return itens;
    }

    @Override
    public List<Item> listByIdColecao(Integer idColecao) {
        ArrayList<Item> itens = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(CONNECTION_STRING, USER, PASS)) {
            StringBuilder sql = new StringBuilder();
            sql.append(" SELECT ");
            sql.append(getItemColumns());
            sql.append(" FROM ITEM WHERE ID_COLECAO = ? ");

            PreparedStatement stmt = conn.prepareStatement(sql.toString());
            stmt.setInt(1, idColecao);

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Item item = new Item();
                item.setId(rs.getInt("ID_ITEM"));
                item.setIdColecao(rs.getInt("ID_COLECAO"));
                item.setName(rs.getString("NOME"));
                item.setDescription(rs.getString("DESCRICAO"));

                itens.add(item);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return itens;
    }

    @Override
    public Item getById(Integer id) {
        Item item = null;
        try (Connection conn = DriverManager.getConnection(CONNECTION_STRING, USER, PASS)) {
            StringBuilder sql = new StringBuilder();
            sql.append(" SELECT ");
            sql.append(getItemColumns());
            sql.append(" FROM ITEM WHERE ID_ITEM = ? ");

            PreparedStatement stmt = conn.prepareStatement(sql.toString());
            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                item = new Item();
                item.setId(rs.getInt("ID_ITEMS"));
                item.setIdColecao(rs.getInt("ID_COLECAO"));
                item.setName(rs.getString("NOME"));
                item.setDescription(rs.getString("DESCRICAO"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return item;
    }

    @Override
    public Item save(Item item) {
        try (Connection conn = DriverManager.getConnection(CONNECTION_STRING, USER, PASS)) {
            if (item.getId() == null) {
                StringBuilder sql = new StringBuilder();
                sql.append(" INSERT INTO ITEM ( ");
                sql.append(getItemColumns());
                sql.append(" ) VALUES (SEQ_ITEM.NEXTVAL, ?, ?, ?) ");

                PreparedStatement stmt = conn.prepareStatement(sql.toString(), new String[]{"ID_ITEM"});
                stmt.setInt(1, item.getIdColecao());
                stmt.setString(2, item.getName());
                stmt.setString(3, item.getDescription());

                int rowsInserted = stmt.executeUpdate();

                ResultSet generatedKeys = stmt.getGeneratedKeys();
                if (generatedKeys.next()) {
                    item.setId(generatedKeys.getInt(1));
                }
            } else {
                StringBuilder sql = new StringBuilder();
                sql.append(" UPDATE ITEM SET ID_COLECAO = ?, NOME = ?, DESCRICAO = ? WHERE ID_ITEM = ?");

                PreparedStatement stmt = conn.prepareStatement(sql.toString());
                stmt.setInt(1, item.getIdColecao());
                stmt.setString(2, item.getName());
                stmt.setString(3, item.getDescription());
                stmt.setInt(4, item.getId());

                int rowsInserted = stmt.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return item;
    }

    @Override
    public void remove(Integer id) {
        try (Connection conn = DriverManager.getConnection(CONNECTION_STRING, USER, PASS)) {
            StringBuilder sql = new StringBuilder();
            sql.append(" DELETE FROM ITEM WHERE ID_ITEM = ? ");

            PreparedStatement stmt = conn.prepareStatement(sql.toString());
            stmt.setInt(1, id);

            int rowsDeleted = stmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private String getItemColumns() {
        StringBuilder columns = new StringBuilder();
        columns.append(" ID_ITEM , ");
        columns.append(" ID_COLECAO , ");
        columns.append(" NOME , ");
        columns.append(" DESCRICAO ");

        return columns.toString();
    }

}
