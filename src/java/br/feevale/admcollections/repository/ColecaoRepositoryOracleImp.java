package br.feevale.admcollections.repository;

import br.feevale.admcollections.entity.Colecao;
import br.feevale.admcollections.repository.ColecaoRepository;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ColecaoRepositoryOracleImp implements ColecaoRepository {

    private final String CONNECTION_STRING = "jdbc:oracle:thin:@localhost:1521:XE";
    private final String USER = "ADM_COLLECTION";
    private final String PASS = "ADMPASSWORD";

    public ColecaoRepositoryOracleImp() {
        try {
            Class.forName("oracle.jdbc.OracleDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Colecao> listAll() {
        ArrayList<Colecao> colecoes = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(CONNECTION_STRING, USER, PASS)) {
            StringBuilder sql = new StringBuilder();
            sql.append(" SELECT ");
            sql.append(getColecaoColumns());
            sql.append(" FROM COLECAO ");

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql.toString());

            while (rs.next()) {
                Colecao colecao = new Colecao();
                colecao.setId(rs.getInt("ID_COLECAO"));
                colecao.setName(rs.getString("NOME"));
                colecao.setDescription(rs.getString("DESCRICAO"));

                colecoes.add(colecao);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return colecoes;
    }

    @Override
    public Colecao getById(Integer id) {
        Colecao colecao = null;
        try (Connection conn = DriverManager.getConnection(CONNECTION_STRING, USER, PASS)) {
            StringBuilder sql = new StringBuilder();
            sql.append(" SELECT ");
            sql.append(getColecaoColumns());
            sql.append(" FROM COLECAO WHERE ID_COLECAO = ? ");

            PreparedStatement stmt = conn.prepareStatement(sql.toString());
            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                colecao = new Colecao();
                colecao.setId(rs.getInt("ID_COLECAO"));
                colecao.setName(rs.getString("NOME"));
                colecao.setDescription(rs.getString("DESCRICAO"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return colecao;
    }

    @Override
    public Colecao save(Colecao colecao) {
        try (Connection conn = DriverManager.getConnection(CONNECTION_STRING, USER, PASS)) {
            if (colecao.getId() == null) {
                StringBuilder sql = new StringBuilder();
                sql.append(" INSERT INTO COLECAO ( ");
                sql.append(getColecaoColumns());
                sql.append(" ) VALUES (SEQ_COLECAO.NEXTVAL, ?, ?) ");

                PreparedStatement stmt = conn.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
                stmt.setString(1, colecao.getName());
                stmt.setString(2, colecao.getDescription());

                int rowsInserted = stmt.executeUpdate();
                
                ResultSet generatedKeys = stmt.getGeneratedKeys();
                if (generatedKeys.next()) {
                    colecao.setId(generatedKeys.getInt(1));
                }
            } else {
                StringBuilder sql = new StringBuilder();
                sql.append(" UPDATE COLECAO SET NOME = ?, DESCRICAO = ? WHERE ID_COLECAO = ? ");

                PreparedStatement stmt = conn.prepareStatement(sql.toString());
                stmt.setString(1, colecao.getName());
                stmt.setString(2, colecao.getDescription());
                stmt.setInt(2, colecao.getId());
                
                int rowsInserted = stmt.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return colecao;
    }

    @Override
    public void remove(Integer id) {
        try (Connection conn = DriverManager.getConnection(CONNECTION_STRING, USER, PASS)) {
            StringBuilder sql = new StringBuilder();
            sql.append(" DELETE FROM COLECAO WHERE ID_COLECAO = ? ");

            PreparedStatement stmt = conn.prepareStatement(sql.toString());
            stmt.setInt(1, id);

            int rowsDeleted = stmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private String getColecaoColumns() {
        StringBuilder columns = new StringBuilder();
        columns.append(" ID_COLECAO , ");
        columns.append(" NOME , ");
        columns.append(" DESCRICAO ");

        return columns.toString();
    }

}
