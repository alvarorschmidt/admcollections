package br.feevale.admcollections.servlet;

import br.feevale.admcollections.entity.Colecao;
import br.feevale.admcollections.helper.ColecaoHelper;
import br.feevale.admcollections.repository.ColecaoRepository;
import br.feevale.admcollections.repository.ColecaoRepositoryMockImp;
import br.feevale.admcollections.repository.ColecaoRepositoryOracleImp;
import java.io.IOException;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ColecaoServlet extends BaseServlet {
    // https://stackoverflow.com/questions/3541077/design-patterns-web-based-applications

    private final ColecaoRepository colecaoRepository = new ColecaoRepositoryOracleImp();
    
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, RequestVerb verb) throws ServletException, IOException {
        String page = "/WEB-INF/util/page404.jsp";
        String action = getAction(request);

        switch (action) {
            case "buscar":
                page = "/WEB-INF/colecao/lista.jsp";
                request.setAttribute("colecoes", colecaoRepository.listAll());
                break;
            case "cadastrar":
                if (verb.equals(RequestVerb.GET)) {
                    request.setAttribute("submitUrl", request.getContextPath() + "/colecao/cadastrar");
                    page = "/WEB-INF/colecao/detalhe.jsp";
                } else if (verb.equals(RequestVerb.POST)) {
                    Colecao colecao = ColecaoHelper.getColecaoFromRequest(request);
                    colecaoRepository.save(colecao);
                    request.setAttribute("colecoes", colecaoRepository.listAll());
                    page = "/WEB-INF/colecao/lista.jsp";
                }
                break;
            case "atualizar":
                if (verb.equals(RequestVerb.GET)) {
                    Map<String, String> parameters = getQueryParameters(request);
                    try {
                        Integer idColecaoSendoAtualizada = Integer.parseInt(parameters.get("id"));
                        Colecao colecao = colecaoRepository.getById(idColecaoSendoAtualizada);
                        ColecaoHelper.setColecaoOnRequest(colecao, request);
                        request.setAttribute("submitUrl", request.getContextPath() + "/colecao/atualizar");
                        page = "/WEB-INF/colecao/detalhe.jsp";
                    } catch (Exception e) {
                        request.setAttribute("erroMsg", "Nao foi possivel atualizar colecao.");
                    }
                } else if (verb.equals(RequestVerb.POST)) {
                    Colecao colecao = ColecaoHelper.getColecaoFromRequest(request);
                    colecaoRepository.save(colecao);
                    request.setAttribute("colecoes", colecaoRepository.listAll());
                    page = "/WEB-INF/colecao/lista.jsp";
                }
                break;

            case "remover":
                Map<String, String> parameters = getQueryParameters(request);
                try {
                    Integer idColecaoSendoRemovida = Integer.parseInt(parameters.get("id"));
                    colecaoRepository.remove(idColecaoSendoRemovida);
                    page = "/WEB-INF/colecao/lista.jsp";
                    request.setAttribute("colecoes", colecaoRepository.listAll());
                } catch (Exception e) {
                    request.setAttribute("erroMsg", "Nao foi possivel remover colecao.");
                }
                break;
        }

        forwardView(request, response, page);
    }
}
