package br.feevale.admcollections.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HomeServlet extends BaseServlet {
    
    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, RequestVerb verb) throws ServletException, IOException {
        String page = "/WEB-INF/home/home.jsp";
        forwardView(request, response, page);  
    }

}
