package br.feevale.admcollections.servlet;

import br.feevale.admcollections.entity.Item;
import br.feevale.admcollections.helper.ItemHelper;
import br.feevale.admcollections.repository.ColecaoRepository;
import br.feevale.admcollections.repository.ColecaoRepositoryOracleImp;
import br.feevale.admcollections.repository.ItemRepository;
import br.feevale.admcollections.repository.ItemRepositoryOracleImp;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ItemServlet extends BaseServlet {

    private ItemRepository itemRepository = new ItemRepositoryOracleImp();
    private ColecaoRepository colecaoRepository = new ColecaoRepositoryOracleImp();

    @Override
    protected void processRequest(HttpServletRequest request, HttpServletResponse response, RequestVerb verb) throws ServletException, IOException {
        String page = "/WEB-INF/util/page404.jsp";
        String action = getAction(request);
        switch (action) {
            case "buscar":
                if (verb.equals(RequestVerb.GET)) {
                    request.setAttribute("itens", itemRepository.listAll());
                    request.setAttribute("colecoes", colecaoRepository.listAll());
                    request.setAttribute("filtroDeColecaoAtivo", 0);
                    request.setAttribute("submitUrl", request.getContextPath() + "/item/buscar");
                    page = "/WEB-INF/item/lista.jsp";
                } else if (verb.equals(RequestVerb.POST)) {
                    try {
                        Integer idColecaoDoFiltro = Integer.parseInt(request.getParameter("idColecao"));
                        List<Item> itens = idColecaoDoFiltro.compareTo(0) == 0 ? itemRepository.listAll() : itemRepository.listByIdColecao(idColecaoDoFiltro);
                        request.setAttribute("itens", itens);
                        request.setAttribute("colecoes", colecaoRepository.listAll());
                        request.setAttribute("filtroDeColecaoAtivo", idColecaoDoFiltro);
                        request.setAttribute("submitUrl", request.getContextPath() + "/item/buscar");
                        page = "/WEB-INF/item/lista.jsp";
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            case "atualizar":   
                if (verb.equals(RequestVerb.GET)) {
                    Map<String, String> parameters = getQueryParameters(request);
                    try {
                        Integer idItemSendoAtualizado = Integer.parseInt(parameters.get("id"));
                        Item item = itemRepository.getById(idItemSendoAtualizado);
                        ItemHelper.setItemOnRequest(item, request); // TODO, retirar isso do helper e fazer direto aqui.
                        request.setAttribute("submitUrl", request.getContextPath() + "/item/atualizar");
                        page = "/WEB-INF/colecao/detalhe.jsp";
                    } catch (Exception e) {
                        request.setAttribute("erroMsg", "Nao foi possivel atualizar item.");
                    }
                } else if (verb.equals(RequestVerb.POST)) {
                    Item item = ItemHelper.getItemFromRequest(request);
                    itemRepository.save(item);
                    request.setAttribute("itens", itemRepository.listAll());
                    page = "/WEB-INF/item/lista.jsp";
                }
                break;
            case "cadastrar":
                if (verb.equals(RequestVerb.GET)) {
                    request.setAttribute("submitUrl", request.getContextPath() + "/item/cadastrar");
                    request.setAttribute("colecoes", colecaoRepository.listAll());
                    page = "/WEB-INF/item/detalhe.jsp";
                } else if (verb.equals(RequestVerb.POST)) {
                    Item item = ItemHelper.getItemFromRequest(request);
                    itemRepository.save(item);
                    request.setAttribute("itens", itemRepository.listAll());
                    request.setAttribute("colecoes", colecaoRepository.listAll());
                    request.setAttribute("filtroDeColecaoAtivo", 0);
                    request.setAttribute("submitUrl", request.getContextPath() + "/item/buscar");
                    page = "/WEB-INF/item/lista.jsp";
                }
                break;
            case "remover":
                Map<String, String> parameters = getQueryParameters(request);
                try {
                    Integer idItemSendoRemovido = Integer.parseInt(parameters.get("id"));
                    itemRepository.remove(idItemSendoRemovido);
                    request.setAttribute("colecoes", colecaoRepository.listAll());
                    request.setAttribute("filtroDeColecaoAtivo", 0);
                    request.setAttribute("submitUrl", request.getContextPath() + "/item/buscar");
                    page = "/WEB-INF/item/lista.jsp";
                    request.setAttribute("itens", itemRepository.listAll());
                } catch (Exception e) {
                    request.setAttribute("erroMsg", "Não foi possível remover item.");
                }
                break;
        }

        forwardView(request, response, page);
    }

}
