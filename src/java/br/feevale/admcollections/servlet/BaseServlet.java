package br.feevale.admcollections.servlet;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class BaseServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response, RequestVerb.GET);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response, RequestVerb.POST);
    }

    protected abstract void processRequest(HttpServletRequest request, HttpServletResponse response, RequestVerb verb) throws ServletException, IOException;

    protected void forwardView(HttpServletRequest request, HttpServletResponse response, String page) throws ServletException, IOException {
        request.setAttribute("contentPage", page);
        RequestDispatcher view = request.getRequestDispatcher("/WEB-INF/shared/base.jsp");
        view.forward(request, response);
    }

    protected String getAction(HttpServletRequest request) {
        String query = request.getRequestURL().toString();
        int firstIndex = query.lastIndexOf("/") + 1;
        int lastIndex = query.indexOf("?") > -1 ? query.indexOf("?") : query.length();
        
        return query.substring(firstIndex, lastIndex);
    }

    protected Map<String, String> getQueryParameters(HttpServletRequest request) {
        Map<String, String> query_pairs = new LinkedHashMap<String, String>();
        String query = request.getQueryString();
        if (query == null)
            return null;
        String[] pairs = query.split("&");
        
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            query_pairs.put(pair.substring(0, idx), pair.substring(idx + 1));
        }
        return query_pairs;
    }

    protected boolean hasQueryParameters(HttpServletRequest request) {
        return request.getRequestURL().toString().indexOf("?") > -1;
    }

    public static enum RequestVerb {

        GET("get"), POST("post");

        private final String verb;

        RequestVerb(final String verb) {
            this.verb = verb;
        }
    }
}
